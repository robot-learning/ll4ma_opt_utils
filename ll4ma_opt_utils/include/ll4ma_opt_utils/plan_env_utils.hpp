//#include <ll4ma_opt_wrapper/pagmo_opt/wrapper.hpp>


#ifndef LL4MA_COLLISION_MODEL
#define LL4MA_COLLISION_MODEL true
#include <ll4ma_collision_wrapper/scene/kris_ccd/manipulation.h>
namespace coll_ns=kris_ccd_ccheck;
#endif

#include <ll4ma_kdl/manipulator_kdl/robot_kdl.h>

#include <ros/ros.h>
#include <ros/package.h>

// ROS service
#include "ll4ma_opt_utils/UpdateWorld.h"
#include "ll4ma_opt_utils/AddObject.h"

// joint trajectory msg:
#include <trajectory_msgs/JointTrajectory.h>
#include <visualization_msgs/MarkerArray.h>


namespace ll4ma_opt_utils
{
class envUtils
{
public:
  void pub_marker(string uri_file_name, vector<double> pose, string pub_topic,vector<double> color={0.8,0.8,0.8});
  void pub_marker_array(vector<string> uri_file_name, vector<vector<double>> poses, string pub_topic,vector<double> color={0.8,0.8,0.8});
  void init(ros::NodeHandle &n);
  bool init_obj();
  bool add_object_call(ll4ma_opt_utils::AddObject::Request &req, ll4ma_opt_utils::AddObject::Response &res);
  Eigen::Matrix4d get_T(vector<double> pose);
  bool world_call(ll4ma_opt_utils::UpdateWorld::Request &req,ll4ma_opt_utils::UpdateWorld::Response &res);
  vector<double> get_pose(Eigen::Matrix4d T);
  vector<double> get_offset_pose(vector<double> pose_offset,vector<double> pose);
  bool init_env();
  bool init_robot_meshes();

  bool update_obj_pose(vector<double> &obj_pose);
  mutable coll_ns::manipulationEnv c_check;
  ros::NodeHandle* nh;
  ros::Publisher pub,m_pub;
  string base_frame_;
  vector<string> env_mesh;

  mutable manipulator_kdl::robotKDL robot_kdl_;
  vector<double> obj_pose,obj_pose_offset;
  string obj_mesh;
  vector<string> robot_link_names;
  // hashmap:
  std::map<std::string, int> robot_links_hashmap;

  

};
}
