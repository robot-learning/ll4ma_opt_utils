# Robot Trajectory Optimization Utilities

This package contains base classes for prototyping robot motion planning as constrained numerical optimization problems with functions for collision checking and robot kinematics. The code is primarily written in C++. The package is being developed by [Balakumar Sundaralingam](https://balakumar-s.github.io/) at [LL4MA Lab](https://robot-learning.cs.utah.edu), University of Utah.


## License

-   BSD license (refer LICENSE file)


## Requirements

-   Ubuntu 16.04
-   ROS kinetic


## Install instructions

-   Clone this repo into your catkin workspace.
-   catkin compile the package


## Citing

This code was developed as part of the following publication. Please cite the below publication, if you use our source code.

*Sundaralingam B, Hermans T. Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation. Autonomous Robots. 2019 Feb 15;43(2):469-83.*

```
@article{sundaralingam2019relaxed,
  title={Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation},
  author={Sundaralingam, Balakumar and Hermans, Tucker},
  journal={Autonomous Robots},
  volume={43},
  number={2},
  pages={469--483},
  year={2019},
  publisher={Springer}
}
```
