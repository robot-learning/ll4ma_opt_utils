#include <ll4ma_opt_utils/plan_env_utils.hpp>

using namespace ll4ma_opt_utils;
void envUtils::pub_marker(string uri_file_name, vector<double> pose, string pub_topic,vector<double> color)
  {
    m_pub=nh->advertise<visualization_msgs::Marker>(pub_topic, 1);

    visualization_msgs::Marker marker;
    marker.header.frame_id = base_frame_;
    marker.ns = "basic_shapes";
    marker.id = 0;
  
    // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  
    // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    marker.action = visualization_msgs::Marker::ADD;
    
    // Set the pose of the marker.  This is a full 6DOF pose relative to th[e frame/time specified in the header
    marker.pose.position.x = pose[0];
    marker.pose.position.y = pose[1];
    marker.pose.position.z = pose[2];
        if(pose.size()<7)
    {
    tf::Quaternion q;
    q.setRPY(pose[3],pose[4],pose[5]);
    marker.pose.orientation.x = q.x();
    marker.pose.orientation.y = q.y();
    marker.pose.orientation.z = q.z();
    marker.pose.orientation.w = q.w();
    }
    else
    {
      marker.pose.orientation.x = pose[3];
      marker.pose.orientation.y =  pose[4];
      marker.pose.orientation.z =  pose[5];
      marker.pose.orientation.w =  pose[6];

    }
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
    marker.scale.x = 1.0;
    marker.scale.y = 1;
    marker.scale.z = 1;

    // Set the color -- be sure to set alpha to something non-zero!
    marker.color.r = color[0];
    marker.color.g = color[1];
    marker.color.b = color[2];
    marker.color.a = 1.0;

    marker.lifetime = ros::Duration();
    

  // mesh marker:
    uint32_t mesh_shape= visualization_msgs::Marker::MESH_RESOURCE;
    marker.type=mesh_shape;
    marker.mesh_resource=uri_file_name;
    ros::Rate l(10);

    for(int i=0;i<5;++i)
    {
      m_pub.publish(marker);
      l.sleep();
    }

  }
void envUtils::pub_marker_array(vector<string> uri_file_name, vector<vector<double>> poses, string pub_topic,vector<double> color)
  {
    m_pub=nh->advertise<visualization_msgs::MarkerArray>(pub_topic, 1);
    
    visualization_msgs::MarkerArray m_arr;
    for(int i=0;i<uri_file_name.size();++i)
    {
      vector<double> pose=poses[i];
      visualization_msgs::Marker marker;
    
      marker.header.frame_id = base_frame_;
      marker.ns = "basic_shapes";
      marker.id = i;
  
      // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  
      // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
      marker.action = visualization_msgs::Marker::ADD;
    
      // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
      marker.pose.position.x = pose[0];
      marker.pose.position.y = pose[1];
      marker.pose.position.z = pose[2];
      if(pose.size()<7)
      {
        tf::Quaternion q;
        q.setRPY(pose[3],pose[4],pose[5]);
        marker.pose.orientation.x = q.x();
        marker.pose.orientation.y = q.y();
        marker.pose.orientation.z = q.z();
        marker.pose.orientation.w = q.w();
      }
      else
      {
        marker.pose.orientation.x = pose[3];
        marker.pose.orientation.y =  pose[4];
        marker.pose.orientation.z =  pose[5];
        marker.pose.orientation.w =  pose[6];

      }
      // Set the scale of the marker -- 1x1x1 here means 1m on a side
      marker.scale.x = 1.0;
      marker.scale.y = 1;
      marker.scale.z = 1;
      
      // Set the color -- be sure to set alpha to something non-zero!
      marker.color.r = color[0];
      marker.color.g = color[1];
      marker.color.b = color[2];
      marker.color.a = 1.0;
      
      marker.lifetime = ros::Duration();
    

      // mesh marker:
      uint32_t mesh_shape= visualization_msgs::Marker::MESH_RESOURCE;
      marker.type=mesh_shape;
      marker.mesh_resource=uri_file_name[i];
      m_arr.markers.push_back(marker);
    }
      ros::Rate l(10);

    for(int i=0;i<5;++i)
    {
      m_pub.publish(m_arr);
      l.sleep();
    }

  }

void envUtils::init(ros::NodeHandle &n)
{
  nh=&n;
  cerr<<"initializing rob_env_scene"<<endl;
  //coll_ns::manipulationEnv rob_env_checker;
  //c_check=rob_env_checker;
  //cerr<<"copied rob_env_scene"<<endl;
}

bool envUtils::add_object_call(ll4ma_opt_utils::AddObject::Request &req, ll4ma_opt_utils::AddObject::Response &res)
{
    // Load object mesh and pose.
    obj_mesh = req.obj_mesh_file;
    obj_pose.resize(7);
    obj_pose = {req.obj_pose.position.x, req.obj_pose.position.y, req.obj_pose.position.z,
                req.obj_pose.orientation.x, req.obj_pose.orientation.y, req.obj_pose.orientation.z, req.obj_pose.orientation.w};
    obj_pose_offset.resize(7);
    obj_pose_offset = {req.obj_pose_offset.position.x, req.obj_pose_offset.position.y, req.obj_pose_offset.position.z,
                req.obj_pose_offset.orientation.x, req.obj_pose_offset.orientation.y, req.obj_pose_offset.orientation.z, req.obj_pose_offset.orientation.w};

    //std::cout << "Object mesh: " << obj_mesh << std::endl;

    Eigen::Matrix4d t_;
    t_.setIdentity();

    c_check.add_obj_mesh(obj_mesh,t_);
    cout << "Added mesh" << endl;
    
    c_check.update_obj_pose(obj_pose);
    cerr<<"updated mesh position!"<<endl;

    // Add offset to object pose - I'm just following initObj, which doesn't use this updated
    // pose for updating the obj pose, just the visualization...
    vector<double> mesh_pose = get_offset_pose(obj_pose_offset, obj_pose);
    
    // publish environment as marker:
    vector<double> col={0.4,0.4,0.9,1.0};
    pub_marker(ros_uri::file_uri(obj_mesh),obj_pose,"obj_marker", col);

    res.success = true;
    return true;
}

bool envUtils::init_obj()
{

    //vector<double> obj_pose_offset;
    bool success = true;
    //    string obj_mesh;
    success &= nh->getParam("/ll4ma_scene/obj_mesh",obj_mesh);
    success &= nh->getParam("/ll4ma_scene/obj_pose",obj_pose);
    success &= nh->getParam("/ll4ma_scene/obj_pose_offset",obj_pose_offset);

    string stl_file=ros_uri::absolute_path(obj_mesh);

    
    Eigen::Matrix4d t_;
    
    // compute rotation matrix from euler:
    t_.setIdentity();

    /*
    t_(0,3)=obj_pose_offset[0];
    t_(1,3)=obj_pose_offset[1];
    t_(2,3)=obj_pose_offset[2];

    // compute rotation matrix from euler:
    tf::Matrix3x3 R;
    R.setRPY(obj_pose_offset[3],obj_pose_offset[4],obj_pose_offset[5]);
    for (int i=0;i<3;++i)
    {
      t_(i,0)=R[i].x();
      t_(i,1)=R[i].y();
      t_(i,2)=R[i].z();

    }
    */
    // add mesh offset:
    
    c_check.add_obj_mesh(stl_file,t_);
    // update object pose:
    tf::Quaternion q;
    q.setRPY(obj_pose[3],obj_pose[4],obj_pose[5]);
    
    // get quaternion and store as obj_pose:
    obj_pose.resize(7);
    obj_pose[3]=q.x();
    obj_pose[4]=q.y();
    obj_pose[5]=q.z();
    obj_pose[6]=q.w();

    c_check.update_obj_pose(obj_pose);
    //cerr<<"*added object mesh!"<<endl;
    
    vector<double> col={0.4,0.4,0.9,1.0};

    // add offset to object pose:
    //vector<double> mesh_pose=get_offset_pose(obj_pose_offset,obj_pose);
    pub_marker(obj_mesh,obj_pose,"obj_marker",col);

    cout << "Publishing marker." << endl;
    return true;
  }

bool envUtils::update_obj_pose(vector<double> &obj_pose)
{
  c_check.update_obj_pose(obj_pose);
  vector<double> col={0.4,0.4,0.9,1.0};
      
  pub_marker(obj_mesh,obj_pose,"obj_marker",col);

}

Eigen::Matrix4d envUtils::get_T(vector<double> pose)
  {
    Eigen::Matrix4d t_;
    
    // compute rotation matrix from euler:
    t_.setIdentity();
    
    t_(0,3)=pose[0];
    t_(1,3)=pose[1];
    t_(2,3)=pose[2];

    // compute rotation matrix from euler:
    tf::Matrix3x3 R;
    if(pose.size()<7)
    {
      R.setRPY(pose[3],pose[4],pose[5]);
    }
    else
    {
      R.setRotation(tf::Quaternion(pose[3],pose[4],pose[5],pose[6]));
    }
    for (int i=0;i<3;++i)
    {
      t_(i,0)=R[i].x();
      t_(i,1)=R[i].y();
      t_(i,2)=R[i].z();

    }
    return t_;


  }
bool envUtils::world_call(ll4ma_opt_utils::UpdateWorld::Request &req,ll4ma_opt_utils::UpdateWorld::Response &res)
  {
    bool success = true;
    // update world pose if pose is not empty
    if(req.env_poses.size()>0)
    {
      vector<vector<double>> env_poses;
      vector<int> env_ids;
      env_poses.resize(req.env_poses.size());
      for(int i=0;i<req.env_poses.size();++i)
      {
        env_ids.emplace_back(i);
        vector<double> pose;
        pose.resize(7);
        pose[0]=req.env_poses[i].position.x;
        pose[1]=req.env_poses[i].position.y;
        pose[2]=req.env_poses[i].position.z;
        pose[3]=req.env_poses[i].orientation.x;
        pose[4]=req.env_poses[i].orientation.y;
        pose[5]=req.env_poses[i].orientation.z;
        pose[6]=req.env_poses[i].orientation.w;
        env_poses[i]=pose;
      }
      c_check.update_env_poses(env_poses,env_ids);
      pub_marker_array(env_mesh,env_poses,"env_marker");
    }
    // update object pose if pose is not empty
    if(req.obj_pose.orientation.w!=0.0)
    {
        vector<double> pose;
        pose.resize(7);
        pose[0]=req.obj_pose.position.x;
        pose[1]=req.obj_pose.position.y;
        pose[2]=req.obj_pose.position.z;
        pose[3]=req.obj_pose.orientation.x;
        pose[4]=req.obj_pose.orientation.y;
        pose[5]=req.obj_pose.orientation.z;
        pose[6]=req.obj_pose.orientation.w;
        vector<double> col={0.4,0.4,0.9,1.0};

        vector<double> mesh_pose=get_offset_pose(obj_pose_offset,pose);
        c_check.update_obj_pose(mesh_pose);

        // update object pose in environment:
        vector<int> env_ids={env_mesh.size()-1};
        vector<vector<double>> env_pose={mesh_pose};
        c_check.update_env_poses(env_pose,env_ids);
        pub_marker(obj_mesh,mesh_pose,"obj_marker",col);

    }
    res.success=true;
    cerr<<"Updated object pose"<<endl;
    return true;
  }


vector<double> envUtils::get_pose(Eigen::Matrix4d T)
  {
    vector<double> pose;
    pose.resize(7);
    Eigen::Quaterniond q(Matrix3d(T.block(0,0,3,3)));
    pose[0]=T(0,3);
    pose[1]=T(1,3);
    pose[2]=T(2,3);
    
    pose[3]=q.x();
    pose[4]=q.y();
    pose[5]=q.z();
    pose[6]=q.w();
    return pose;
  }
vector<double> envUtils::get_offset_pose(vector<double> pose_offset,vector<double> pose)
  {
    vector<double> m_pose;
    m_pose.resize(7);
    
    Eigen::Matrix4d t_offset=get_T(pose_offset);
    
    Eigen::Matrix4d t_pose=get_T(pose);
    Eigen::Matrix4d t_m=t_pose*t_offset;
    m_pose=get_pose(t_m);
    
    return m_pose;
  }

bool envUtils::init_env()
  {
    vector<bool> cvx_idx;//={true,false};
    bool success=true;
    // load environment mesh:
    success &= nh->getParam("/ll4ma_scene/env_mesh",env_mesh);
    std::cout << success << std::endl;
    
    vector<double> pose;
    success &= nh->getParam("/ll4ma_scene/env_pose",pose);

    vector<string> f_names;
    for(int i=0;i<env_mesh.size();++i)
    {
      string stl_file=ros_uri::absolute_path(env_mesh[i]);
      f_names.emplace_back(stl_file);
      cvx_idx.emplace_back(true);

    }
      
    if(!success)
    {
      ROS_ERROR("environment scene failed to load");
      return false;
    }
        
    //    string obj_mesh;
    // success &= nh->getParam("/ll4ma_scene/obj_mesh",obj_mesh);
    // success &= nh->getParam("/ll4ma_scene/obj_pose",obj_pose);
    // success &= nh->getParam("/ll4ma_scene/obj_pose_offset",obj_pose_offset);

    // string obj_stl_file=ros_uri::absolute_path(obj_mesh);

    // // add mesh offset:
    
    // // update object pose:
    // tf::Quaternion q;
    // q.setRPY(obj_pose[3],obj_pose[4],obj_pose[5]);
    
    // // get quaternion and store as obj_pose:
    // obj_pose.resize(7);
    // obj_pose[3]=q.x();
    // obj_pose[4]=q.y();
    // obj_pose[5]=q.z();
    // obj_pose[6]=q.w();

    // vector<double> mesh_pose=get_offset_pose(obj_pose_offset,obj_pose);
        
    // f_names.emplace_back(obj_stl_file);
    // cvx_idx.emplace_back(false);
    //vector<string> f_names={stl_file,obj_stl_file};
    
    vector<Eigen::Matrix4d> t_(env_mesh.size()+1,Eigen::Matrix4d::Identity());
    //cerr<<t_[0]<<endl;

    // Add, then update pose of, environment mesh (assumed single).
    c_check.add_env_meshes(f_names,t_,cvx_idx);
    std::vector<int> ids = {0};
    std::vector<std::vector<double>> poses = {pose};
    c_check.update_env_poses(poses, ids);
    
    ROS_INFO("added environment mesh!");
    // publish environment as marker:
    pub_marker(env_mesh[0],pose,"env_marker");
    //pub_marker(env_mesh[0],pose,"env_marker");


    return true;
  }
bool envUtils::init_robot_meshes()
  {
    
    // read parameters from rosparam server:
    
    vector<string> arm_link_names;
    vector<string> hand_link_names;
    string urdf_param;

    int n_links;
    // default params:
    vector<string> ee_names;//={"palm_link","index_tip","middle_tip","rin_tip","thumb_tip"};
    vector<string> base_names;//;={"base_link","base_link","base_link","base_link","base_link"};
    vector<string> j_names;
    bool success = true;
    success &= nh->getParam("urdf_param",urdf_param);
    
    while(!nh->hasParam(urdf_param) && ros::ok())
    {
    }
    
    // read parameters from rosparam server:
    success &= nh->getParam("base_frames",base_names);
    base_frame_=base_names[0];
    success &= nh->getParam("ee_frames",ee_names);
    success &= nh->getParam("joint_names",j_names);
    success &= nh->getParam("arm_coll_links",arm_link_names);
    success &= nh->getParam("hand_coll_links",hand_link_names);
    // store indexing of collision links and contact links:
    // we store collision checking links first and then contact links;
    
    robot_link_names=arm_link_names;
    robot_link_names.insert(robot_link_names.end(),hand_link_names.begin(),hand_link_names.end());

    // store hashmap for robot_links:
    for(int i=0;i<robot_link_names.size();++i)
    {
      robot_links_hashmap[robot_link_names[i]]=i;
      
    }
    //robot_link_names.insert(robot_link_names.end(),hand_contact_link_names.begin(),hand_contact_link_names.end());

    // build collision meshes:
    std::string robot_desc_string;
    
    // read urdf as a string:
    nh->param(urdf_param, robot_desc_string, std::string());
    c_check.load_robot_links(arm_link_names,hand_link_names,robot_desc_string);

    ROS_INFO("Created robot collision objects");

    
    // build kdl model:
    vector<double> g_vec={0.0,0.0,-9.8};
    robot_kdl_=manipulator_kdl::robotKDL(urdf_param,*nh,base_names,ee_names,robot_link_names,g_vec);
    
    robot_kdl_.getJointLimits(j_names,robot_kdl_.up_bounds,robot_kdl_.low_bounds);
    success &= robot_kdl_.updateJointStateNames(j_names);
    success &= robot_kdl_.generateChainJointidx();
    /*
    //cerr<<robot_kdl_.up_bounds.size()<<" "<<robot_kdl_.dof<<endl;
    
     // for debugging..
    for (int i=0;i<robot_kdl_.up_bounds.size();++i)
    {
      cerr<<" "<<robot_kdl_.up_bounds[i];
    }
    
    */
    // load robot reachability meshes:    
    cerr<<"Created robot kdl object"<<endl;
    if(!success)
    {
      ROS_ERROR("Failed to load robot model!");
      return false;
    }
    return true;
  }

