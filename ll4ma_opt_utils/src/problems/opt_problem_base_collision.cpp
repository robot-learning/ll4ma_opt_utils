#ifndef LL4MA_COLLISION_MODEL
#define LL4MA_COLLISION_MODEL true
#include <ll4ma_collision_wrapper/scene/kris_ccd/manipulation.h>
namespace coll_ns=kris_ccd_ccheck;
#endif

#include <ll4ma_opt_utils/problems/opt_problem_base.hpp>

namespace opt_problems
{
#if LL4MA_COLLISION_MODEL==true
bool optProblemBase::updateCollisionModel(coll_ns::manipulationEnv &c_check)
{
  c_checker=c_check;
  return true;
}
#endif
}
