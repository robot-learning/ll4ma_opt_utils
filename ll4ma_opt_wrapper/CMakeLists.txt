cmake_minimum_required(VERSION 3.2)
project(ll4ma_opt_wrapper)

# set flags for building wrappers:

# NLOPT pagmo flag
add_definitions(-DBUILD_PAGMO_NLOPT)
# SNOPT pagmo flag
add_definitions(-DBUILD_PAGMO_SNOPT)
# worhp pagmo flag
#add_definitions(-DBUILD_PAGMO_WORHP)


## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

find_package(catkin REQUIRED
  roscpp
  std_msgs
  roslib
  )


#set(BOOST_ROOT $ENV{HOME}/local/)
find_package(Boost 1.58 REQUIRED COMPONENTS system filesystem)
find_package(Pagmo REQUIRED)

## Generate added messages and services with any dependencies listed here

catkin_package(
  INCLUDE_DIRS include
  DEPENDS Boost
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}/include
  ${Pagmo_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  $ENV{HOME}/pkgs/pagmo_plugins_nonfree/include
  )



###########
## Build ##
###########
# create list of pagmo linking libs:
set(PAGMO_LIBS Pagmo::pagmo  ${Boost_FILESYSTEM_LIBRARIES}  ${Boost_SYSTEM_LIBRARIES}  ${CMAKE_DL_LIBS})

# TODO
# add snopt solver as a library
#add_library(snopt_opt STATIC src/snopt/solver.cpp)

# testing flags:
option(PACKAGE_TESTS "Build the tests" ON)
if(PACKAGE_TESTS)
    enable_testing()
    add_subdirectory(test)
endif()


