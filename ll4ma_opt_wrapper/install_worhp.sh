#!/usr/bin/env bash
set -e
INSTALL_PATH="$HOME/local"

SRC_PATH="$HOME/pkgs"

# To use SNOPT: 
INSTALL_PAGMO_WORHP_PLUGIN=true

TEST_WORHP_PAGMO=false

# pamgo_plugins_non_free requires dll header files from boost 1.61.
INSTALL_BOOST_DLL=true

## copy dll include files from Boost 1.61 to system boost(works with 1.58):
if $INSTALL_BOOST_DLL; then
    sudo cp -a dll/ /usr/include/boost/dll
fi


## install pagmo_plugins_nonfree
if $INSTALL_PAGMO_WORHP_PLUGIN; then
    cd $SRC_PATH && git clone https://github.com/balakumar-s/pagmo_plugins_nonfree.git && cd pagmo_plugins_nonfree  &&   mkdir build && cd build && cmake .. && make 
fi

# If everything is installed, you can test if snopt is running by the below commands:
# You should have no errors/failures from these tests
if $TEST_WORHP_PAGMO; then
    cd $SRC_PATH && cd pagmo_plugins_nonfree/build/tests && echo "***Running pagmo linking test:\n"
    ./snopt7
    echo "***RUNNNING SNOPT7 library runtime linking test:" && ./snopt7_lib_test
fi
