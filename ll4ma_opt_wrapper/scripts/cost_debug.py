# this script can be used to plot costs
import numpy as np
import matplotlib.pyplot as plt


def nl_cost(x,thresh):
    return np.power(max(x,thresh)-thresh,2)
if __name__=='__main__':
    thresh=2.0
    cost_data=np.array(range(1000))*0.01
    nl_cost_data=[]
    for i in range(len(cost_data)):
        nl_cost_data.append(nl_cost(cost_data[i],thresh))
    nl_cost_data=np.ravel(nl_cost_data)
    # plot:
    
    plt.plot(cost_data,nl_cost_data)
    plt.show()
