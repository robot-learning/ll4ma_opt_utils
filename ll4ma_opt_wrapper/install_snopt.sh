#!/usr/bin/env bash
set -e
INSTALL_PATH="$HOME/local"

SRC_PATH="$HOME/pkgs"

# To use SNOPT: 
INSTALL_SNOPT=true
INSTALL_SNOPT_INTERFACE=true
INSTALL_PAGMO_SNOPT_PLUGIN=true

TEST_SNOPT_PAGMO=true

# pamgo_plugins_non_free requires dll header files from boost 1.61.
INSTALL_BOOST_DLL=true

## copy dll include files from Boost 1.61 to system boost(works with 1.58):
if $INSTALL_BOOST_DLL; then
    sudo cp -a dll/ /usr/include/boost/dll
fi

## install snopt:
# Download snopt7 source files into $SRC_PATH
if $INSTALL_SNOPT; then
    cd $SRC_PATH && cd snopt7 && ./configure --prefix=/usr/local/ && make && sudo make install
fi

## install snopt-interface:
if $INSTALL_SNOPT_INTERFACE; then
    cd $SRC_PATH && git clone https://github.com/balakumar-s/snopt-interface.git && cd snopt-interface && git checkout ll4ma_pkgs &&
	autoconf && ./configure --with-c-cpp --prefix=/usr/local/ && make && sudo make install
fi


## install pagmo_plugins_nonfree
if $INSTALL_PAGMO_SNOPT_PLUGIN; then
    cd $SRC_PATH && git clone https://github.com/balakumar-s/pagmo_plugins_nonfree.git && cd pagmo_plugins_nonfree  &&   mkdir build && cd build && cmake .. && make 
fi

# If everything is installed, you can test if snopt is running by the below commands:
# You should have no errors/failures from these tests
if $TEST_SNOPT_PAGMO; then
    cd $SRC_PATH && cd pagmo_plugins_nonfree/build/tests && echo "***Running pagmo linking test:\n"
    ./snopt7
    echo "***RUNNNING SNOPT7 library runtime linking test:" && ./snopt7_lib_test
fi
