#include "pagmo_headers.hpp"
#include <vector>
//#include <string>
#include <ros/ros.h>
#include <ros/package.h>

#include <gtest/gtest.h>

// call snopt opt function:

#ifdef BUILD_PAGMO_SNOPT
#include <pagmo_plugins_nonfree/snopt7.hpp>

double snopt_call()
{
  using namespace pagmo;

  problem prob{constrained_function{2}};
  //problem prob{rosenbrock{2}};
  //problem prob{cec2006{7u}};
  population pop{prob, 10u, 23u};
  algorithm algo{snopt7(false,"/usr/local/lib/")};
  algo.set_verbosity(1u);


  pop = algo.evolve(pop);
  std::vector<double> f_best;
  f_best=pop.champion_f();
  /*
  for (int i=0;i<x_best.size();++i)
  {
    std::cerr<<x_best[i]<<std::endl;
  }
  */
  return f_best[0];
  
}

TEST(ll4ma_opt_wrapper,pagmo_snopt)
{
  ASSERT_EQ(4.0,snopt_call());  
} 
#endif

#ifdef BUILD_PAGMO_NLOPT
double nlopt_call()
{
  using namespace pagmo;
  
  problem prob{constrained_function{2}};
  //problem prob{rosenbrock{2}};
  //problem prob{cec2006{7u}};
  population pop{prob, 10u, 23u};
  // call nlopt slsqp
  algorithm algo{nlopt("slsqp")};

  algo.set_verbosity(100u);


  pop = algo.evolve(pop);
  std::vector<double> f_best;
  f_best=pop.champion_f();
  return f_best[0];
  
}


TEST(ll4ma_opt_wrapper,pagmo_nlopt)
{
  ASSERT_EQ(4.0,nlopt_call());  
} 

#endif

#ifdef BUILD_PAGMO_WORHP
#include <pagmo_plugins_nonfree/worhp.hpp>

double worhp_call()
{
  using namespace pagmo;
  
  problem prob{constrained_function{2}};
  //problem prob{rosenbrock{2}};
  //problem prob{cec2006{7u}};
  population pop{prob, 10u, 23u};
  // call nlopt slsqp

  algorithm algo{worhp(true,"/usr/lib/libworhp.so")};

  algo.set_verbosity(100u);


  pop = algo.evolve(pop);
  std::vector<double> f_best;
  f_best=pop.champion_f();
  return f_best[0];
  
}


TEST(ll4ma_opt_wrapper,pagmo_worhp)
{
  ASSERT_EQ(4.0,worhp_call());  
} 

#endif


