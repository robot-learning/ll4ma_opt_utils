#include "snopt/solver.h"

using namespace snopt;
solver::solver()
{
  DEBUG("Initializing snopt solver class");


  // problem variables:
  
  int start_type,n_F,n,obj_row,*x_state,*F_state, n_s, n_inf, *iA_fun, *jA_var,ne_A,*IG_fun,*jG_var,ne_G;
  double *x_low,*x_upp,*F_low,*F_upp, *x, *F,obj_add,*A,*F_mul,*x_mul,s_inf;
  snFunA usr_fun;

  // variables:
  

  
  //problem.solve()
}
void solver::demo()
{
  DEBUG("Running Demo");
  // Allocate and initialize;
  int n     =  2;
  int neF   =  3;

  int nS = 0, nInf;
  double sInf;

  double *x      = new double[n];
  double *xlow   = new double[n];
  double *xupp   = new double[n];
  double *xmul   = new double[n];
  int    *xstate = new    int[n];

  double *F      = new double[neF];
  double *Flow   = new double[neF];
  double *Fupp   = new double[neF];
  double *Fmul   = new double[neF];
  int    *Fstate = new int[neF];

  int    ObjRow  = 0;
  double ObjAdd  = 0;

  int Cold = 0, Basis = 1, Warm = 2;


  // Set the upper and lower bounds.
  xlow[0]   =  0.0;  xlow[1]   = -1e20;
  xupp[0]   = 1e20;  xupp[1]   =  1e20;
  xstate[0] =    0;  xstate[1] =  0;

  Flow[0] = -1e20; Flow[1] = -1e20; Flow[2] = -1e20;
  Fupp[0] =  1e20; Fupp[1] =   4.0; Fupp[2] =  5.0;
  Fmul[0] =   0;   Fmul[0] =   0;   Fmul[0] =    0;
  x[0]    = 1.0;
  x[1]    = 1.0;


  // snopta will compute the Jacobian by finite-differences.
  // snJac will be called  to define the
  // coordinate arrays (iAfun,jAvar,A) and (iGfun, jGvar).

  // Reset the variables and solve ...

  int lenA   = 6;
  int *iAfun = new int[lenA];
  int *jAvar = new int[lenA];
  double *A  = new double[lenA];

  int lenG   = 6;
  int *iGfun = new int[lenG];
  int *jGvar = new int[lenG];

  int neA, neG; // neA and neG must be defined when providing dervatives

  xstate[0] =   0;  xstate[1] = 0;
  Fmul[0]   =   0;  Fmul[0]   = 0; Fmul[0] =    0;
  x[0]      = 1.0;
  x[1]      = 1.0;


  // Provide the elements of the Jacobian explicitly.
  iGfun[0] = 1;
  jGvar[0] = 0;

  iGfun[1] = 1;
  jGvar[1] = 1;

  iGfun[2] = 2;
  jGvar[2] = 0;

  iGfun[3] = 2;
  jGvar[3] = 1;
  neG      = 4;

  iAfun[0] = 0;
  jAvar[0] = 1;
  A[0]     = 1.0;
  neA      = 1;
  snoptProblemA ToyProb;

  ToyProb.setSpecsFile   ("sntoya.spc");
  ToyProb.setIntParameter("Derivative option", 1);
  ToyProb.setIntParameter("Major Iteration limit", 250);
  ToyProb.setIntParameter("Verify level ", 3);
  /*
  snFunA usrfun;
  usrfun=obj_function;

  ToyProb.solve(Cold, neF, n, ObjAdd, ObjRow,usrfun,		iAfun, jAvar, A, neA,		iGfun, jGvar, neG,
		xlow, xupp, Flow, Fupp,
		x, xstate, xmul,
		F, Fstate, Fmul,
		nS, nInf, sInf);

  */
  for (int i = 0; i < n; i++){
    cout << "x = " << x[i] << " xstate = " << xstate[i] << endl;
  }
  for (int i = 0; i < neF; i++){
    cout << "F = " << F[i] << " Fstate = " << Fstate[i] << endl;
  }
  
}


void solver::obj_function(int *Status,int *n, double x[],int *needF,int *neF,double F[],
                          int *needG, int *neG, double G[],
                          char *cu, int *lencu,
                          int iu[],int *lenio,
                          double ru[],int *lenru)
{
  if(*needF>0)
  {
    // compute F
    F[0]=x[1];
    F[1] =  x[0]*x[0] + 4*x[1]*x[1];
    F[2] = (x[0] - 2)*(x[0] - 2) + x[1]*x[1];    
  }
  if(*needG>0)
  {
    // compute gradient
     // iGfun[0] = 1
    // jGvar[0] = 0
    G[0] = 2*x[0];

    // iGfun[1] = 1
    // jGvar[1] = 1
    G[1] = 8*x[1];

    // iGfun[2] = 2
    // jGvar[2] = 0
    G[2] = 2*(x[0] - 2);

    // iGfun[3] = 2
    // jGvar[3] = 1
    G[3] = 2*x[1];
  }

}


