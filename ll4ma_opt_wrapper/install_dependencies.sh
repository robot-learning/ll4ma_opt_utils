#!/usr/bin/env bash
set -e
# ========================================================================================== #
# Installs the dependencies to a desired filepath. 
#                                                                                            #
#                                                       
# ========================================================================================== #

INSTALL_PATH="$HOME/local"
ROS_VER="melodic"
SRC_PATH="$HOME/pkgs"


# Not required for ubuntu 16.04:
INSTALL_EIGEN=false # ubuntu 16.04 already has eigen 3.2


# To use SLSQP:(uncomment below flags)
INSTALL_NLOPT=true
INSTALL_PAGMO_NLOPT=true

# To use SNOPT: run instal_snopt.sh script

# TODO: setup IPOPT install
# INSTALL_IPOPT=false

# ========================================================================================== #

WORKSPACE=$WS_PATH/

mkdir -p $SRC_PATH
mkdir -p $INSTALL_PATH

cd $SRC_PATH


# Get dependencies:



## Eigen:
if $INSTALL_EIGEN; then
    cd $SRC_PATH
    wget -O eigen_3_2_10.tar.gz http://bitbucket.org/eigen/eigen/get/3.2.10.tar.gz  &&
	mkdir eigen_3_2_10 && tar -xvzf eigen_3_2_10.tar.gz -C eigen_3_2_10 --strip-components 1 &&
	rm eigen_3_2_10.tar.gz &&
	cd eigen_3_2_10 && mkdir build && cd build &&
	cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH &&
	make -j8 && make install
fi

## nlopt:

if $INSTALL_NLOPT; then
    cd $SRC_PATH
    wget http://ab-initio.mit.edu/nlopt/nlopt-2.4.2.tar.gz &&
	tar -xvzf nlopt-2.4.2.tar.gz &&
	rm nlopt-2.4.2.tar.gz &&
	cd nlopt-2.4.2 && ./configure &&
	make -j8 && sudo make install
fi

## install pagmo with NLOPT:
if $INSTALL_PAGMO_NLOPT; then
    cd $SRC_PATH && git clone https://github.com/balakumar-s/pagmo2.git &&
	cd pagmo2 &&
	mkdir -p build && cd build && cmake .. -DPAGMO_WITH_NLOPT=ON -DPAGMO_WITH_EIGEN3=ON -DPAGMO_BUILD_PAGMO=ON && make -j8 && sudo make install    
fi





## ipopt:
#if $INSTALL_IPOPT; then
#    wget https://www.coin-or.org/download/source/Ipopt/Ipopt-3.2.1.tgz &&
#	tar -xvzf Ipopt-3.2.1.tgz && rm Ipopt-3.2.1.tgz &&
#	cd Ipopt-3.2.1.tgz 
#fi





