// This file creates a
#include<iostream>

#include "debug.h"

// snopt header files
#include <snopt/snopt.h>
#include <snopt/snoptProblem.hpp>


using namespace std;

namespace snopt
{
class solver
{
public:
  solver();// constructor
  void demo();// tests a simple problem
  // Public variables:
  
  //snoptProblemA ToyProb;
  
  static void obj_function(int *Status,int *n, double x[],int *needF,int *neF,double F[],
                    int *needG, int *neG, double G[],
                    char *cu, int *lencu,
                    int iu[],int *lenio,
                    double ru[],int *lenru);
};
};

