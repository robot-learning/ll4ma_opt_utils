#ifdef DEBUG_FLAG
#undef DEBUG_FLAG
#endif

#ifdef DEBUG_ALL_FLAG
#undef DEBUG_ALL_FLAG
#endif

// uncomment below line to enable debug information
//#define DEBUG_FLAG 1

//#define DEBUG_ALL 1 // Prints out information inside every loop (a lot). Only for Lt. Commander Data.



#ifdef DEBUG_FLAG
#define DEBUG(x) do { std::cerr << x <<std::endl; } while (0)
#endif
#ifndef DEBUG_FLAG
#define DEBUG(x) 
#endif

// Usage in code:
//  DEBUG("text"<<var<<"more text");

// custom assert macros
//RUNTIME_ASSERT(
