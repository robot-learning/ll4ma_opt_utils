#ifndef LL4MA_PAGMO_SAMPLE_PROBLEM
#define LL4MA_PAGMO_SAMPLE_PROBLEM
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <pagmo/exceptions.hpp>
#include <pagmo/io.hpp>
#include <pagmo/problem.hpp>
#include <pagmo/types.hpp>


namespace pagmo
{
  /*
    Constrained function:
    min F(X)
    s.t. G(X)<=0

  */
  struct constrained_function{
    constrained_function(vector_double::size_type dim=2,vector_double::size_type nic=0,vector_double::size_type nec=1) : m_dim(dim), m_nic(nic), m_nec(nec)
    {
    };

    // Fitness computation
    // This is the objective function+constraints giving the total cost
    vector_double fitness(const vector_double &x) const
    {
      double retval=0.0;
      retval=x[0]*x[0]+x[1]*x[1];
      double ineq_1;
      
      double eq_1;
      eq_1=x[0]-2.0;
      return {retval,eq_1};// return {obj_fn,equality_constraints,inequality_constraints}
    
    }

    // Bounds(variable bounds)
    // This function encodes the constraints
    std::pair<vector_double,vector_double> get_bounds() const
    {
      vector_double lb(m_dim,-15);
      vector_double ub(m_dim,15);
      return {lb,ub};
    }

    // returns the number of inequality constraints
    vector_double::size_type get_nic() const
    {
      return m_nic;
    }

    // returns the number of equality constraints
    vector_double::size_type get_nec() const
    {
      return m_nec;
    }

      
    // Problem name

    std::string get_name() const
    {
      return "Contrained Optimization Problem";
    }

    vector_double gradient ( const vector_double &x) const
    {
      vector_double retval(m_dim+m_dim*m_nec+m_dim*m_nic,0.0);
      retval[0]=2.0*x[0];
      retval[1]=2.0*x[1];
      retval[2]=1.0;
      return retval;
    }
    
    // optimal solution:

    vector_double best_known() const
    {
      return vector_double(m_dim,1.);// Maybe initialize with current state?
    }


    // problem dimension
    vector_double::size_type m_dim;
    vector_double::size_type m_nec;
    vector_double::size_type m_nic;
  };
}
#endif
