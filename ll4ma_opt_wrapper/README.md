# Numerical optimization wrapper package

This catkinized package wraps around numerical optimization solvers for solving constrained optimization problems and is primarily written in C++. The package is being developed by [Balakumar Sundaralingam](https://balakumar-s.github.io/) at [LL4MA Lab](https://robot-learning.cs.utah.edu), University of Utah.


## License

-   BSD license (refer LICENSE file)


## Solvers/Wrappers

This package currently wraps around [PAGMO](https://esa.github.io/pagmo2/) and has access to

-   NLOPT
-   SNOPT (requires license).
-   WORHP (requires license).


## Requirements

-   Ubuntu 16.04
-   ROS kinetic


## Install instructions

-   Clone this repo into your catkin workspace.
-   Run install\_dependencies.sh script to install the dependencies. **Ubuntu 16**: https://bitbucket.org/robot-learning/ll4ma_opt_utils/issues/5/incompatibility-of-install_dependencies
-   If you have a SNOPT licence, run "instal\_snopt.sh".
-   catkin compile the package
-   run 'rosrun ll4ma\_opt\_wrapper test\_all', the tests should all pass.


## Citing

This code was developed as part of the following publication. Please cite the below publication, if you use our source code.

*Sundaralingam B, Hermans T. Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation. Autonomous Robots. 2019 Feb 15;43(2):469-83.*

```
@article{sundaralingam2019relaxed,
  title={Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation},
  author={Sundaralingam, Balakumar and Hermans, Tucker},
  journal={Autonomous Robots},
  volume={43},
  number={2},
  pages={469--483},
  year={2019},
  publisher={Springer}
}
```
