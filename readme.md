# LL4MA Optimization Software

This package contains research code to work with optimization problems commonly used for robot motion planning. The package is being developed by [Balakumar Sundaralingam](https://balakumar-s.github.io/) at [LL4MA Lab](https://robot-learning.cs.utah.edu), University of Utah.


## Contributors

-   [Balakumar Sundaralingam](<https://balakumar-s.github.io/>)


## License

-   BSD license (refer LICENSE file)


## Sub Packages


## ll4ma\_opt\_wrapper

-   Wraps around numerical optimization solvers
-   More info: [README](md_ll4ma_opt_wrapper_README.html)


## ll4ma\_opt\_utlis

-   Enables easy prototyping by linking with robot kinematic, dynamic functions and also collision checking libraries.
-   More info: [README](md_ll4ma_opt_utils_README.html)


## External Dependencies

-   [ll4ma\_collision\_wrapper](https://bitbucket.org/robot-learning/ll4ma_collision_wrapper)
-   [ll4ma\_kdl](https://bitbucket.org/robot-learning/ll4ma_kdl)


## Citing

This code was developed as part of the following publication. Please cite the below publication, if you use our source code.

*Sundaralingam B, Hermans T. Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation. Autonomous Robots. 2019 Feb 15;43(2):469-83.*

```
@article{sundaralingam2019relaxed,
  title={Relaxed-rigidity constraints: kinematic trajectory optimization and collision avoidance for in-grasp manipulation},
  author={Sundaralingam, Balakumar and Hermans, Tucker},
  journal={Autonomous Robots},
  volume={43},
  number={2},
  pages={469--483},
  year={2019},
  publisher={Springer}
}
```
